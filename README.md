Exchange:
===========

Proyecto que nace con el objetivo de crear wallet para criptomonedas.

Directorios:
============

Dentro de la aplicación conviven dos ambientes distintos, una de backend y la otra de frontend, en la raiz del proyecyo estan diferenciados:
    - Exchange (Backend)
    - ExchangeFrontend (Front) 

Instalacion del Proyecto modo Desarrollo:
=========================================


Para instalar la apliacacion en modo desarrollo debera seguir los siguientes pasos:

1-) Instalar el controlador de versiones git:
---------------------------------------------
    
    $ su

    # aptitude install git

2-) Descargar el codigo fuente del proyecto Exchange:
-------------------------------------------------------

    Para descargar el código fuente del proyecto contenido en su repositorio GIT realice un clon del proyecto Exchange, con el siguiente comando:

    $ git clone https://gitlab.com/rud-130/Exchange.git

3-) Instalar Backend (Exchange/):
------------------------------

Entrar en la carpeta Exchange/ y seguir los siguientes pasos:

3.1-) Crear un Ambiente Virtual:
------------------------------

    El proyecto está desarrollado con el lenguaje de programación Python, se debe instalar Python v3.4.2. Con los siguientes comandos puede instalar Python y PIP.

    Entrar como root para la instalacion 

    # aptitude install python3.4 python3-pip python3.4-dev python3-setuptools

    # aptitude install python3-virtualenv virtualenvwrapper

    Salir del modo root y crear el ambiente:

    $ mkvirtualenv --python=/usr/bin/python3 Exchange

3.2-) Instalar los requerimientos del proyecto:
----------------------------------------------

    Para activar el ambiente virtual Exchange ejecute el siguiente comando:

    $ workon Exchange

    (Exchange)$

    Entrar en la carpeta raiz del proyecto:

    (Exchange)$ cd Exchange
    (Exchange)Exchange$ 

    Desde ahi se deben instalar los requirimientos del proyecto con el siguiente comando:

    (Exchange)$ pip install -r requirements.txt

    De esta manera se instalaran todos los requerimientos iniciales para montar el proyecto 
    

3.4-) Crear base de datos y Migrar los modelos:
---------------------------------------------

    El manejador de base de datos que usa el proyecto es postgres, antes de la creación de la base de datos, se deba asegurar que postgres este instalado correctamente en su equipo.

    Para crear la base de datos desde postgres se ingresa a la consola interactiva de postgres y se ejecuta la siguiente sentencia:

    postgres=# CREATE DATABASE exchange OWNER=postgres ENCODING='UTF−8';

    Para migrar los modelos del proyecto se debe usar el siguiente comando:

    (Exchange)$ python manage.py makemigrations
    (Exchange)$ python manage.py migrate

3.5-) Cargar data inicial del proyecto:
---------------------------------------------

    Asegurese de que los modelos esten migrados en base de datos y ejecute los siguientes comando para cargar la data inicial del proyecto:

    Colocar aqui las initial data de la plataforma

    (Exchange)$  python manage.py loaddata fixtures/...json


3.6-) Iniciar la aplicación Exchange:
--------------------------------------------

    Para iniciar la apliación se debe  ejecutar el siguiente comando:

    (Exchange)$ python manage.py runserver

	Ingresar a la plataforma en la direccion local:

	url: http://localhost:8000/api

4-) Instalar Fronted (ExchangeFrontend/):
--------------------------------------------

Entrar en la carpeta ExchangeFrontend/ y seguir los siguientes pasos:


4.1-) Instalar los requerimientos del proyecto:
----------------------------------------------

    Dentro de la carpeta se debe ejecutar el comando

    npm install


4.2-) Configurar url para servir:
----------------------------------------------

    En la ruta ExchangeFrontend/package.json, en la línea 7 tenemos el siguiente
    fragmento:

    "start": "ng serve --host 192.168.18.164"

    La dirección ip *192.168.18.164* debemos cambiarla por la deseada o removerla
    si queremos escuchar en el localhost


4.3-) Iniciar la aplicación Exchange:
--------------------------------------------

    Para iniciar la apliación se debe  ejecutar el siguiente comando:

    npm start

    Ingresar a la plataforma en la direccion local:

    url: http://direccion_ip_configurada_arriba:3000


Contactos: 
----------

    Leonel Paolo Hernandez Macchiarulo (leonelphm@gmail.com)

    Rodrigo Alejandro Boet Da Costa (rudmanmrrod@gmail.com)
    
