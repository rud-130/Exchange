import { Component, OnInit } from '@angular/core';

import { MxnValue } from './prices.models';
import { PriceService } from './prices.service';

@Component({
  selector: 'app-prices',
  templateUrl: './prices.component.html',
  styleUrls: ['./prices.component.css'],
  providers: [PriceService]
})
export class PricesComponent implements OnInit {

	mxnValue: MxnValue = {
		btc: 0,
    eth: 0,
    bch: 0,
    xrp: 0,
    usd: 0,
	}
	
  constructor(private priceService: PriceService) { }

  ngOnInit() {
    this.getMxnValue();
    let obj = this;
    setInterval(function(){
      obj.getMxnValue();
    }, 10000);
  }

  getMxnValue(){
    this.priceService.getMxnToCm().subscribe(response => {
      this.mxnValue.btc = response['btc'];
      this.mxnValue.bch = response['bch'];
      this.mxnValue.eth = response['eth'];
      this.mxnValue.xrp = response['xrp'];
      this.mxnValue.usd = response['usd'];
    },err => {
      console.error('Oops:', err.message);
    });
  }

}
