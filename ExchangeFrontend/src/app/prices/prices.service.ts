import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';


import { RouterApi } from '../app.service';


@Injectable()
export class PriceService {

  constructor(private http: HttpClient, private routerApi: RouterApi) {
  }

  
 getMxnToCm(): Observable<Object>{ 
    let url = this.routerApi.apiMxnValue();
    let header = this.routerApi.createHeader();
    return this.http.get(url,{headers:header});
  }

}