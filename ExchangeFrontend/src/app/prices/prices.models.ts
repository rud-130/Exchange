export class MxnValue {
    btc: Number;
    eth: Number;
    bch: Number;
    xrp: Number;
    usd: Number;
}