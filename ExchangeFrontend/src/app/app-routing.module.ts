import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HomeComponent } from './home/home.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { WalletComponent } from './wallet/wallet.component';
import { ProfileComponent } from './profile/profile.component';
import { ConfigurationComponent } from './configuration/configuration.component';
import { PricesComponent } from './prices/prices.component';
import { GraphicsComponent } from './graphics/graphics.component';

const routes: Routes = [
	{ path: 'dashboard', component: DashboardComponent, 
    children:[
      { path: '', component: WalletComponent },
      { path: 'profile', component: ProfileComponent },
      { path: 'configuration', component: ConfigurationComponent },
      { path: 'prices', component: PricesComponent },
      { path: 'graphics', component: GraphicsComponent},
    ]
  },
  { path: '', component: HomeComponent },
  { path: '**', component: HomeComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
