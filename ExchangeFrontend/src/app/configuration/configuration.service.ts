import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';


import { RouterApi } from '../app.service';


@Injectable()
export class ConfigurationService {

  constructor(private http: HttpClient, private routerApi: RouterApi) {
  }

  
  getMyCurrentDevice(params): Observable<Object>{ 
    let url = this.routerApi.apiMyCurrentDevice();
    let header = this.routerApi.createHeader();
    return this.http.post(url,params,{headers:header})
  }

  createDevice(params): Observable<Object>{ 
    let url = this.routerApi.apiMyDevices();
    let header = this.routerApi.createHeader();
    return this.http.post(url,params,{headers:header})
  }

  listDevice(params): Observable<Object>{ 
    let url = this.routerApi.apiMyDevices();
    url += '?=pk'+params.pk
    let header = this.routerApi.createHeader();
    return this.http.get(url,{headers:header})
  }

}