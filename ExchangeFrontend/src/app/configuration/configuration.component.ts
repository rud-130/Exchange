import { Component, OnInit } from '@angular/core';

import { ConfigurationService } from './configuration.service';
import { MyCurrentDevice } from './configuration.models';

@Component({
  selector: 'app-configuration',
  templateUrl: './configuration.component.html',
  styleUrls: ['./configuration.component.css'],
  providers: [ConfigurationService]
})
export class ConfigurationComponent implements OnInit {
	myCurrentDevice: MyCurrentDevice = {
  	ip: '',
    name: '',
    browser: ''
  }
  myDevices: any;

  constructor(private configurationService: ConfigurationService) { }

  ngOnInit() {
  	this.getMyCurrentDevice();
  	this.listDevices();
  }

  getMyCurrentDevice(){
  	let user = JSON.parse(localStorage.getItem('user'));
  	let params = {'pk':user.pk};
  	this.configurationService.getMyCurrentDevice(params).subscribe(response => {
      this.myCurrentDevice.ip = response['ip'];
      this.myCurrentDevice.name = response['nombre_equipo'];
      this.myCurrentDevice.browser = response['navegador'];
    },err => {
      console.error('Oops:', err.message);
    });
  }

  addDevice(){
  	let user = JSON.parse(localStorage.getItem('user'));
  	let params = {'pk':user.pk,'direccion_ip':this.myCurrentDevice.ip,
  	'acceso':true,'nombre':this.myCurrentDevice.name,'navegador':this.myCurrentDevice.browser};
  	this.configurationService.createDevice(params).subscribe(response => {
      console.log(response);
      this.listDevices();
    },err => {
      console.error('Oops:', err.message);
    });
  }

  listDevices(){
  	let user = JSON.parse(localStorage.getItem('user'));
  	let params = {'pk':user.pk};
  	this.configurationService.listDevice(params).subscribe(response => {
      this.myDevices = response;
    },err => {
      console.error('Oops:', err.message);
    });

  }

}
