import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Profile, ChangePassword, ChangePasswordValidate } from './profile.models';
import { ProfileService } from './profile.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css'],
  providers: [ProfileService],
})
export class ProfileComponent implements OnInit {
	tipo_identificador: any[] = [
		{id:'DNI',name:'dni'},
		{id:'PASS',name:'pasaporte'},
	];

	country: any;
  profile_exist: Boolean;

	profileModel: Profile = {
    username: '',
    email: '',
    first_name: '',
    last_name: '',
    pais: '',
    ciudad: '',
    tipo_indentificador: '',
    identificador: '',
    zip_code: '',
    direccion: ''
  };

  changepassModel: ChangePassword = {
  	password: '',
    new_password: '',
    new_password_repeat: '',
  }

  changePasswordValidate: ChangePasswordValidate ={
    password: '',
    password_isInvalid: false,
    new_password: '',
    new_password_isInvalid: false,
    new_password_repeat: '',
    new_password_repeat_isInvalid: false,
  }

  constructor(private router: Router, private profileService: ProfileService) {
    this.profile_exist = false;
  }

  ngOnInit() {
    if(!localStorage.getItem('token')){
      this.router.navigate(['home']);
    }
  	this.getCountries();
  	this.getProfile();
  }

  getProfile(){
  	let user = JSON.parse(localStorage.getItem('user'));
  	this.profileModel.username = user.username
  	this.profileModel.email = user.email
  	this.profileModel.first_name = user.first_name
  	this.profileModel.last_name = user.last_name

  	this.profileService.getData({pk:user.pk}).subscribe(response => {
      if(Object.keys(response).length>0){
        this.profile_exist = true;
    		localStorage.setItem('profile_id',response['pk'])
    		this.profileModel.pais = response['fk_pais'];
        this.profileModel.tipo_indentificador = response['tipo_indentificador'];
        this.profileModel.identificador = response['identificador'];
        this.profileModel.ciudad = response['ciudad'];
        this.profileModel.zip_code = response['zip_code'];
        this.profileModel.direccion = response['direccion'];
      }
    },err => {
      console.error('Oops:', err.message);
    });
  }

  updateProfile(){
		let profile = localStorage.getItem('profile_id');
    let user = JSON.parse(localStorage.getItem('user'));
		let params = {
      'pk':user.pk,
			'pk_user':profile,
			'fk_pais':this.profileModel.pais,
			'tipo_identificador':this.profileModel.tipo_indentificador,
			'identificador':this.profileModel.identificador,
      'ciudad':this.profileModel.ciudad,
			'zip_code':this.profileModel.zip_code,
			'direccion':this.profileModel.direccion
		};
		this.profileService.postData(params,this.profile_exist).subscribe(response => {
      console.log(response);
    },err => {
      console.error('Oops:', err.message);
    });
  }

  setCountry(id: any): void {
		console.log(id);
  }

  getCountries(){
  	this.profileService.getCountry().subscribe(response => {
      this.country = response;
      console.log(this.country);
    },err => {
      console.error('Oops:', err.message);
    });
  }

  setIdentificador(id: any): void {
		console.log(id);
  }

  changePassword(){
  	if(this.change_valid()){
  		let user = JSON.parse(localStorage.getItem('user'));
  		let params = {
  			'pk_user':user.pk,
  			'password_old':this.changepassModel.password,
  			'password_new':this.changepassModel.new_password
  		};
  		this.profileService.postChangePassword(params).subscribe(response => {
	      console.log(response);
	    },err => {
	      console.error('Oops:', err.message);
	    });
  	}
  }

  change_valid(){
    let valid = true;
    let required = "Este campo es requerido";
    if(this.changepassModel.password==''){
      this.changePasswordValidate.password = required;
      this.changePasswordValidate.password_isInvalid = true;
      valid = false
    }
    else{
      this.changePasswordValidate.password_isInvalid = false;
    }
    if(this.changepassModel.new_password==''){
      this.changePasswordValidate.new_password = required;
      this.changePasswordValidate.new_password_isInvalid = true;
      valid = false
    }
    else{
      this.changePasswordValidate.new_password_isInvalid = false;
    }
    if(this.changepassModel.new_password_repeat==''){
      this.changePasswordValidate.new_password_repeat = required;
      this.changePasswordValidate.new_password_repeat_isInvalid = true;
      valid = false
    }
    else{
      this.changePasswordValidate.new_password_repeat_isInvalid = false;
    }
    if(this.changepassModel.new_password!=this.changepassModel.new_password_repeat){
    	this.changePasswordValidate.new_password_repeat = 'Las contraseñas no coinciden';
      this.changePasswordValidate.new_password_repeat_isInvalid = true;
    }
    else{
      this.changePasswordValidate.new_password_repeat_isInvalid = false;
    }
    return valid;
  }



}
