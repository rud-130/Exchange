import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { RouterApi } from '../app.service';


@Injectable()
export class ProfileService {

  constructor(private http: HttpClient, private routerApi: RouterApi) {
  }

  
 getData(params): Observable<Object>{ 
    let url = this.routerApi.apiProfile();
    let header = this.routerApi.createHeader();
    url += '?pk='+params.pk;
    return this.http.get(url,{headers:header});
  }

  postData(params,update): Observable<any[]>{ 
    let header = this.routerApi.createHeader();
    let response;
    if(update){
      let url = this.routerApi.apiProfile();
      url += params['pk_user']+"/";
      let response = this.http.put(url,params,{headers:header})

    }
    else{
      let url = this.routerApi.apiProfile();
      let response = this.http.post(url,params,{headers:header})
    }
    return response;
  }

  getCountry(): Observable<Object>{ 
    let url = this.routerApi.apiCountry();
    let header = this.routerApi.createHeader();
    return this.http.get(url,{headers:header});
  }

  postChangePassword(params): Observable<Object>{ 
    let url = this.routerApi.apiChangePassword();
    let header = this.routerApi.createHeader();
    return this.http.post(url,params,{headers:header})
  }

}