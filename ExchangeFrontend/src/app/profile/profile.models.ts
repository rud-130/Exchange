export class Profile {
	pais: String;
    ciudad: String;
    tipo_indentificador: String;
    identificador: String;
    zip_code: String;
    direccion: String;
    username: String;
    first_name: String;
    last_name: String;
    email: String;
}

export class ChangePassword {
    password: String;
    new_password: String;
    new_password_repeat: String;
}

export class ChangePasswordValidate {
    password: String;
    password_isInvalid: Boolean;
    new_password: String;
    new_password_isInvalid: Boolean;
    new_password_repeat: String;
    new_password_repeat_isInvalid: Boolean;
}