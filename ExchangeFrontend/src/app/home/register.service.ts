import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';


import { RouterApi } from '../app.service';


@Injectable()
export class RegisterService {

  constructor(private http: HttpClient, private routerApi: RouterApi) {
  }

  
 register(params): Observable<Object>{ 
    let url = this.routerApi.apiRegister();
    return this.http.post(url,params)
  }


}