import { Component, OnInit, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';
//import {MaterializeAction} from "angular2-materialize";

import { Login, Register, RegisterValidate,LoginValidate } from './home.models';
import { RegisterService } from './register.service';
import { LoginService } from '../login/login.service';


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
  providers: [LoginService, RegisterService]
})
export class HomeComponent implements OnInit {
  //signup = new EventEmitter<string|MaterializeAction>();
  //login = new EventEmitter<string|MaterializeAction>();
  //login_step = new EventEmitter<string|MaterializeAction>();
  login_active = true;
  loginModel: Login = {
  	username: '',
  	password: '',
    code: '',
  };
  registerModel: Register = {
    username: '',
    email: '',
    password: '',
    password_repeat: '',
    first_name: '',
    last_name: ''
  };
  registerValidate: RegisterValidate = {
    username: '',
    username_isInvalid: false,
    email: '',
    email_isInvalid: false,
    password: '',
    password_isInvalid: false,
    password_repeat: '',
    password_repeat_isInvalid: false,
    first_name: '',
    first_name_isInvalid: false,
    last_name: '',
    last_name_isInvalid: false
  };
  loginValidate: LoginValidate ={
    username: '',
    username_isInvalid: false,
    password: '',
    password_isInvalid: false,
    code: '',
    code_isInvalid: false,
  }

  constructor(private loginService: LoginService, private registerService: RegisterService,
  private router: Router) { }

  ngOnInit() {
  }

  openRegister(){
  	//this.signup.emit({action:"modal",params:['open']});
  }

  closeRegister(){
  	//this.signup.emit({action:"modal",params:['close']});
  }

  openLogin(){
  	//this.login.emit({action:"modal",params:['open']});
  }

  closeLogin(){
  	//this.login.emit({action:"modal",params:['close']});
  }

  makeLogin(){
    if(this.login_active){
      if(this.validateLogin_step1()){
        let params = {'username':this.loginModel.username,'password':this.loginModel.password};
        console.log(params);
        this.loginService.loginValidate(params).subscribe(response => {
          this.login_active = false;
        },err => {
          console.error('Oops:', err.message);
        });
      }
    }
    else{
      if(this.validateLogin_step2()){
        let params = {'username':this.loginModel.username,'token':this.loginModel.code};
        this.loginService.loginCode(params).subscribe(response1 => {
          localStorage.setItem('user',JSON.stringify(response1['user']))
          let params = {'username':this.loginModel.username,'password':this.loginModel.password};
          this.loginService.login(params).subscribe(response2 => {
            localStorage.setItem('token',response2['token'])
            this.login_active = true;
            this.loginModel.username = '';
            this.loginModel.password = '';
            this.loginModel.code = '';
            this.closeLogin();
            this.router.navigate(['dashboard']);
          });
        });
      }
    }
  }

  backLogin(){
    this.login_active = true;
  }

  validateLogin_step1(){
    let valid = true;
    let required = "Este campo es requerido";
    if(this.loginModel.username==''){
      this.loginValidate.username = required;
      this.loginValidate.username_isInvalid = true;
      valid = false
    }
    else{
      this.loginValidate.username_isInvalid = false;
    }
    if(this.loginModel.password==''){
      this.loginValidate.password = required;
      this.loginValidate.password_isInvalid = true;
      valid = false
    }
    else{
      this.loginValidate.password_isInvalid = false;
    }
    return valid;
  }

  validateLogin_step2(){
    let valid = true;
    if(this.loginModel.code==''){
      this.loginValidate.code = "Este campo es requerido";
      this.loginValidate.code_isInvalid = true;
      valid = false
    }
    else{
      this.loginValidate.code_isInvalid = false;
    }
    return valid;
  }

  validateRegister(){
    let valid = true;
    let required = "Este campo es requerido";
    if(this.registerModel.username==''){
      this.registerValidate.username = required;
      this.registerValidate.username_isInvalid = true;
      valid = false
    }
    else{
      this.registerValidate.username_isInvalid = false;
    }
    if(this.registerModel.email==''){
      this.registerValidate.email = required;
      this.registerValidate.email_isInvalid = true;
      valid = false
    }
    else{
      let emailPattern = new RegExp(/^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/);
      if(!emailPattern.test(String(this.registerModel.email))){
        valid = false
        this.registerValidate.email = "El formato es inválido";
        this.registerValidate.email_isInvalid = true;
      }
      else{
        this.registerValidate.email_isInvalid = false;
      }
    }
    if(this.registerModel.password==''){
      this.registerValidate.password = required;
      this.registerValidate.password_isInvalid = true;
      valid = false
    }
    else{
      this.registerValidate.password_isInvalid = false;
    }
    if(this.registerModel.password_repeat==''){
      this.registerValidate.password_repeat = required;
      this.registerValidate.password_repeat_isInvalid = true;
      valid = false
    }
    else{
      this.registerValidate.password_repeat_isInvalid = false;
    }
    if(this.registerModel.first_name==''){
      this.registerValidate.first_name = required;
      this.registerValidate.first_name_isInvalid = true;
      valid = false
    }
    else{
      this.registerValidate.first_name_isInvalid = false;
    }
    if(this.registerModel.last_name==''){
      this.registerValidate.last_name = required;
      this.registerValidate.last_name_isInvalid = true;
      valid = false
    }
    else{
      this.registerValidate.last_name_isInvalid = false;
    }
    if(this.registerModel.password_repeat!=this.registerModel.password){
      this.registerValidate.password_repeat = "Las contraseñas no coinciden";
      this.registerValidate.password_repeat_isInvalid = true;
      valid = false
    }
    else{
      this.registerValidate.password_repeat_isInvalid = false;
    }
    return valid;
  }

  makeValid(){
    this.registerValidate.username_isInvalid = false;
    this.registerValidate.email_isInvalid = false;
    this.registerValidate.password_isInvalid = false;
    this.registerValidate.password_repeat_isInvalid = false;
    this.registerValidate.first_name_isInvalid = false;
    this.registerValidate.last_name_isInvalid = false;
  }

  makeRegister(){
    if(this.validateRegister()){
      this.makeValid();
      let params = {username: this.registerModel.username,
        email: this.registerModel.email,
        password: this.registerModel.password,
        first_name: this.registerModel.first_name,
        last_name: this.registerModel.last_name
      };
      this.registerService.register(params).subscribe(response => {
        console.log(response);
      });
    }
  }

}
