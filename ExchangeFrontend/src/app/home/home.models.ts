export class Login {
    username: String;
    password: String;
    code: String;
}

export class LoginValidate {
    username: String;
    username_isInvalid: Boolean;
    password: String;
    password_isInvalid: Boolean;
    code: String;
    code_isInvalid: Boolean;
}

export class Register {
    username: String;
    email: String;
    password: String;
    password_repeat: String;
    first_name: String;
    last_name: String;
}

export class RegisterValidate {
    username: String;
    username_isInvalid: Boolean;
    email: string;
    email_isInvalid: Boolean;
    password: String;
    password_isInvalid: Boolean;
    password_repeat: String;
    password_repeat_isInvalid: Boolean;
    first_name: String;
    first_name_isInvalid: Boolean;
    last_name: String;
    last_name_isInvalid: Boolean;
}
