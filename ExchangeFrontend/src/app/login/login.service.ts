import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';


import { RouterApi } from '../app.service';


@Injectable()
export class LoginService {

  constructor(private http: HttpClient, private routerApi: RouterApi) {
  }

  
 login(params): Observable<Object>{ 
    let url = this.routerApi.apiLogin();
    return this.http.post(url,params);
  }

  loginValidate(params): Observable<Object>{ 
    let url = this.routerApi.apiLoginValidate();
    return this.http.post(url,params);
  }

  loginCode(params): Observable<Object>{ 
    let url = this.routerApi.apiLoginCode();
    return this.http.post(url,params)
  }


  logout(){
    localStorage.removeItem('token');
  }


}