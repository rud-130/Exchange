import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';

import { RouterApi } from './app.service';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { LoginComponent } from './login/login.component';
import { ProfileComponent } from './profile/profile.component';
import { PricesComponent } from './prices/prices.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { ConfigurationComponent } from './configuration/configuration.component';
import { WalletComponent } from './wallet/wallet.component';
import { GraphicsComponent } from './graphics/graphics.component';

import { ChartModule } from 'angular-highcharts';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MzNavbarModule, MzParallaxModule, MzCardModule, 
  MzModalModule, MzButtonModule, MzDropdownModule,
  MzSidenavModule, MzInputModule, MzIconMdiModule,
  MzSelectModule, MzIconModule } from 'ngx-materialize';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    LoginComponent,
    ProfileComponent,
    PricesComponent,
    DashboardComponent,
    ConfigurationComponent,
    WalletComponent,
    GraphicsComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    ChartModule,
    BrowserAnimationsModule,
    MzNavbarModule,
    MzParallaxModule,
    MzCardModule,
    MzModalModule,
    MzButtonModule,
    MzDropdownModule,
    MzSidenavModule,
    MzInputModule,
    MzIconMdiModule,
    MzSelectModule,
    MzIconModule,
  ],
  providers: [RouterApi],
  bootstrap: [AppComponent]
})
export class AppModule { }
