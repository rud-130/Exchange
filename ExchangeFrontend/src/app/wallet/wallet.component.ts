import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { Currency } from './wallet.models';

@Component({
  selector: 'app-wallet',
  templateUrl: './wallet.component.html',
  styleUrls: ['./wallet.component.css']
})
export class WalletComponent implements OnInit {
  currencyModel: Currency = {
  	name: '',
  	value: 0,
  };

  constructor(private router: Router) {  }

  ngOnInit() {
    if(!localStorage.getItem('token')){
      this.router.navigate(['home']);
    }
  	this.getBtcValue();
  }

  getBtcValue(){
  	this.currencyModel.name = 'BTC';
  	this.currencyModel.value = 0;
  }

  getEthValue(){
  	this.currencyModel.name = 'ETH';
  	this.currencyModel.value = 0;
  }

  getBchValue(){
  	this.currencyModel.name = 'BCH';
  	this.currencyModel.value = 0;
  }

  getPetValue(){
  	this.currencyModel.name = 'PET';
  	this.currencyModel.value = 0;
  }

  getMxnValue(){
  	this.currencyModel.name = 'MXN';
  	this.currencyModel.value = 0;
  }

  getBsValue(){
  	this.currencyModel.name = 'BS';
  	this.currencyModel.value = 0;
  }

}

