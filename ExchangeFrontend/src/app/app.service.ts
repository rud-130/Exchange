import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';


@Injectable()
export class RouterApi {
  BASE_URL : String;

  constructor() {
    this.BASE_URL = 'http://192.168.18.164:8000/api/';
  }

    apiLoginValidate(){ 
        return this.BASE_URL + 'login/one-time/';
    }

    apiLoginCode(){ 
        return this.BASE_URL + 'login/validate/';
    }

    apiLogin(){ 
        return this.BASE_URL + 'api-token-auth/';
    }

    apiRefresh(){ 
      return this.BASE_URL + 'auth/';
    }

    apiRegister(){ 
      return this.BASE_URL + 'auth/users/create/';
    }

    apiProfile(){ 
      return this.BASE_URL + 'register/users/';
    }

    apiChangePassword(){ 
      return this.BASE_URL + 'user/change-password/';
    }

    apiCountry(){ 
      return this.BASE_URL + 'list/pais/';
    }

    // CONFIGS URLS

    apiMyCurrentDevice(){ 
      return this.BASE_URL + 'user/config/';
    }

    apiMyDevices(){ 
      return this.BASE_URL + 'dispositivos/';
    }

    // APIS URLS
    apiMxnValue(){ 
      return this.BASE_URL + 'precios/';
    }


    createHeader() {
      let headers = new HttpHeaders({
          'Authorization': 'JWT ' + localStorage.getItem('token')
        })
      return headers;
    }
}