# -*- coding: utf8 -*-

"""Python module for interacting with a Bitcoin Node
"""

import logging
import requests
from requests import exceptions
from requests import codes
import json
from django.conf import settings

from .passwordgenerator import password_generator


class BitcoinBlockchainAdapter():
    """Class to manage requests to the Bitcoin Blockchain.
    """

    def __init__(self):
        """Returns a BitcoinBlockchainAdapter object.

        Parameter
        ---------
        xxx: `str`

        xxx: `str`

        """

        self._internal_response = {}
        self._internal_response['responseCode'] = None
        self._internal_response['message'] = None
        self._internal_response['blockchainResponse'] = None

    
    def _set_internal_response(self, rCode, msg, bResponse):
        """Set the internalResponse values.

        Parameters
        ----------
        rCode: `int`
            HTML status code returned by Blockchain.info API
        msg: `str`
            Message returned by Blockchain.info API in case of raising an 
            APIException
        bResponse: `dict`
            dictionary with information requested
            
        """
        self._internal_response['responseCode'] = rCode
        self._internal_response['message'] = msg
        self._internal_response['blockchainResponse'] = bResponse

    def create_wallet(self):
        """Creates a HD wallet.
        """
        # we have to specify the password to protect the wallet
        password = password_generator()
        bitcoin_wallet = {}
        self._set_internal_response(200, '', bitcoin_wallet)
        return self._internal_response                    

    def get_hd_account_balance(self, guid, password, xpub):
        """Returns account balance
        """
        pass

    def wallet_balance(self, guid, password, accounts):
        """Returns wallet's balance.

        Parameters
        ----------
        - guid : `str` 
            wallet id
        - password : `str`
            wallet password
        - accounts : `queryset`
            queryset of wallet's accounts

        """ 
        balance = 0
        for account in accounts:
            response_balance = get_hd_account_balance(guid, password, account.xpub)
            print(response_balance)
            if response_balance['responseCode'] == 200:
                balance = balance + response_balance['blockchainResponse']['balance']                
                self._set_internal_response(200, '', response_balance['blockchainResponse'])
            else: 
                self._set_internal_response(response_balance['responseCode'], response_balance['message'], {'balance':-1})
                return self._internal_response
        self._set_internal_response(200, '', response_balance['blockchainResponse'])
        return self._internal_response

    def generate_receiving_address(self, xpub):
        """        
        """
        pass

    def receiving_address(self, xpub):
        """Returns a receiving address

        Parameters
        ----------
        - xpub : `str`
            extended public key 
        Returns
        -------

        """
        response_address = generate_receiving_address(xpub)
        if response_address['responseCode'] == 200:
            self._set_internal_response(200, '', response_address['blockchainResponse'])
        else:
            print("******response_address['message'] ")
            print(response_address['message'])
            self._set_internal_response(response_address['responseCode'], response_address['message'], {})
        return self._internal_response

    def create_hd_account(self, guid, password):
        """Creates a HD account in a wallet

        """
        new_account = {}
        self._set_internal_response(200, '', new_account['blockchainResponse'])
        return self._internal_response

    def monitor_receiving_address(self, address):
        """Enables monitoring address for incoming transactions.

        Paramaters
        ----------
        - address : `str`
            address to be monitored
        Returns
        -------

        """
        address='1B5iEH1ccRpVnM51KbsaMBgLydD5n4Hhz9'
        response_monitoring = {}
        self._set_internal_response(200, '', response_monitoring['blockchainResponse'])
        return self._internal_response



    def send_money(self, guid, password, to, amount, from_):
        """Sends bitcoins to the specified address

        Parameters
        ----------
        - guid : `str`
            id of the wallet
        - amount : `int`
            amount to be sent
        - to : `str`
            address to send bitcoins

        Returns
        -------

        """        
        response_send = {}
        if response_send['responseCode'] == 200:
            self._set_internal_response(200, '', response_send['blockchainResponse'])
        else:
            self._set_internal_response(response_send['responseCode'], response_send['message'], {})
        return self._internal_response






