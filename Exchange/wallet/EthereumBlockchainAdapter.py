# -*- coding: utf8 -*-

"""Python module for interacting with a Ethereum Node
"""

import logging
import requests
from requests import exceptions
from requests import codes
import json
from django.conf import settings

from web3 import Web3, HTTPProvider

from .passwordgenerator import password_generator


class EthereumBlockchainAdapter():
    """Class to manage requests to the Ethereum Blockchain.

    There are two types of Ethereum accounts: externally owned account (EOA) 
    and contracts account. This adapter will manage Externally Owned Account 
    that is defined by a pair of keys, a private and a public key. Accounts 
    are indexed by their address which is derived from the public key by 
    taking the last 20 bytes.

    The EthereumBlockchainAdapter will use the Web3.py Python library for 
    interacting with Ethereum.
    """

    def __init__(self):
        """Returns a EthereumBlockchainAdapter object.

        Parameter
        ---------
        xxx: `str`

        xxx: `str`

        """

        RPC_ADDRESS = ''
        """Ethereum network and access token
        """

        self._internal_response = {}
        self._internal_response['responseCode'] = None
        self._internal_response['message'] = None
        self._internal_response['blockchainResponse'] = None

        self._web3 = Web3(HTTPProvider(RPC_ADDRESS))
    
    def _set_internal_response(self, rCode, msg, bResponse):
        """Set the internalResponse values.

        Parameters
        ----------
        rCode: `int`
            HTML status code
        msg: `str`
            Message returned by Ethereum API in case of an error
        bResponse: `dict`
            dictionary with information requested
            
        """
        self._internal_response['responseCode'] = rCode
        self._internal_response['message'] = msg
        self._internal_response['blockchainResponse'] = bResponse

    def create_wallet(self):
        """Creates an Ethereum HD wallet

        """
        # we have to specify the password to protect the wallet
        password = password_generator()
        response = self._web3
        pass

    
    def wallet_balance(self, guid, password, xpub):
        """Returns wallet's balance.

        """ 
        pass
