"""
Exchange platform
"""
## @package wallet.models
#
# Model that builds util data models
# @author Ing. Leonel P. Hernandez M.
# @version 1.0
from django.contrib.auth.models import (
    User
)
from django.db import models


class CryptoWallet(models.Model):
    """!
    Clase que contiene las apis de acceso para la wallet

    @author Ing. Leonel P. Hernandez M. (leonelphm at gmail.com)
    @copyright
    @date 14-04-2018
    @version 1.0.0
    """
    nombre = models.CharField(max_length=50)
    api_access = models.CharField(max_length=128)
    active = models.BooleanField(default=True)

    class Meta:
        """!
            Clase que construye los meta datos del modelo
        """
        ordering = ('nombre',)
        verbose_name = 'Crypto Wallet'
        verbose_name_plural = 'Cryptos Wallets'
        db_table = 'wallet_crypto_wallet'

    def __str__(self):
        """!
        Función que muestra el nombre de la api

        @return Devuelve el username del usuario
        """
        return self.nombre


class WalletUser(models.Model):
    """!
    Clase que contiene las  wallets de los usaurios

    @author Ing. Leonel P. Hernandez M. (leonelphm at gmail.com)
    @copyright
    @date 14-04-2018
    @version 1.0.0
    """
    fk_user = models.ForeignKey(User, on_delete=models.CASCADE)
    fk_wallet = models.ForeignKey(CryptoWallet, on_delete=models.CASCADE)
    public_hash = models.CharField(max_length=300)
    # credencial para entrar al wallet
    password = models.CharField(max_length=128, null=True)
    # saldo de la criptomoneda
    balance_coin =  models.DecimalField(max_digits=20, decimal_places=8)
    active = models.BooleanField(default=True)

    class Meta:
        """!
            Clase que construye los meta datos del modelo
        """
        ordering = ('fk_user',)
        verbose_name = 'Wallet User'
        verbose_name_plural = 'Wallets Users'
        db_table = 'wallet_wallet_user'

    def __str__(self):
        """!
        Función que muestra el wallet del usurio

        @return Devuelve el username del usuario
        """
        return self.fk_user.username
