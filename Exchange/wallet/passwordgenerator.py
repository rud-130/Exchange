# -*- coding: utf8 -*-

import string
import random

#def password_generator(size=20, chars=string.ascii_letters + string.digits + string.punctuation):
def password_generator(size=20, chars=string.ascii_letters + string.digits):
    """
    Returns a string of random characters, useful in generating temporary
    passwords for automated password resets.
    
    size: default=8; override to provide smaller/larger passwords
    chars: default=A-Za-z0-9; override to provide more/less diversity
    
    Credit: Ignacio Vasquez-Abrams
    Source: http://stackoverflow.com/a/2257449
    """
    random_string = ''.join(random.choice(chars) for i in range(size))
    #print (random_string)
    return random_string
