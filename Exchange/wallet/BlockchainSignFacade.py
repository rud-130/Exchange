# -*- coding: utf8 -*-

"""Python module for interacting with a Blockchain Adapter
"""
import urllib

from django.core.exceptions import ObjectDoesNotExist

from .constants import *
from .passwordgenerator import password_generator
from BitcoinBlockchainAdapter import BitcoinBlockchainAdapter


class BlockchainSignFacade():
    """!
    Class to manage requests to a specific BlockChainAdapter.

    @author Antonio Araujo (a.araujo at @eicabmx.com)
    @copyright 
    @date 15-05-2017
    @version 1.0.0
    """
    
    def __init__(self):
        """Returns a BlockchainSignFacade object.

        Parameter
        ---------
        xxx: `str`

        xxx: `str`

        """
                
        self._bitcoin_adapter = BitcoinBlockchainAdapter()
        """Bitcoin adapter
        """

        self._ethereum_adapter = None
        """Ethereum adapter
        """

        self._bitcoincash_adapter = None
        """Bitcoin Cash adapter
        """

        self._litecoin_adapter = None
        """Litecoin adapter
        """
    
    def hello(self):
        """Just say hello
        """
        data = {}
        data['msg']='hello'

        return data

    def create_wallet(self, cryptocurrency, label=None):
        """Creates a wallet for a specific cryptocurrency
        """
        wallet = {}
        wallet['blockchainResponse'] = {}
        wallet['responseCode'] = 400
        wallet['message'] = ''
        if cryptocurrency == 'BTC':
            wallet = self._bitcoin_adapter.create_wallet()
        elif cryptocurrency == 'ETH':            
            wallet['message'] = CRYPTOCURRENCY_NOT_SUPPORTED
            #response['ethereum'] = ethereum_wallet
        elif cryptocurrency == 'BCH':
            wallet['message'] = CRYPTOCURRENCY_NOT_SUPPORTED
            #response['bitcoin_cash'] = bitcoincash_wallet
        elif cryptocurrency == 'LTC':
            wallet['message'] = CRYPTOCURRENCY_NOT_SUPPORTED
            #response['litecoin'] = litecoin_wallet
        else:
            wallet['message'] = CRYPTOCURRENCY_NOT_SUPPORTED
        return wallet


    def wallet_balance(self, cryptocurrency, wallet_id):
        """Returns wallet balance

        Parameters
        ----------
        currency : `str`
            Cryptocurrency: BTC, ETH, BCH, LTC
        wallet_id : `str`
            Wallet id

        Returns
        -------

        """
        balance = {}
        balance['blockchainResponse'] = {}
        balance['responseCode'] = 400
        balance['message'] = ''
        if cryptocurrency == 'BTC':
            try:
                print ('...getting wallet %s' % wallet_id)
                #bitcoin_wallet = BitcoinWallet.objects.get(pk=wallet_id)
                #accounts = BitcoinAccount.objects.filter(fk_bitcoin_wallet=bitcoin_wallet.id)
                #balance = self._bitcoin_adapter.wallet_balance(bitcoin_wallet.guid, bitcoin_wallet.password, accounts)
            except ObjectDoesNotExist:
                print ('WALLET NOT FOUND')
                balance['message'] = WALLET_NOT_FOUND
        elif cryptocurrency == 'ETH':            
            balance['message'] = CRYPTOCURRENCY_NOT_SUPPORTED
        elif cryptocurrency == 'BCH':
            balance['message'] = CRYPTOCURRENCY_NOT_SUPPORTED
        elif cryptocurrency == 'LTC':
            balance['message'] = CRYPTOCURRENCY_NOT_SUPPORTED
        else:
            balance['message'] = CRYPTOCURRENCY_NOT_SUPPORTED
        return balance


    def address_wallet(self, cryptocurrency):
        """Returns a new address to receive payment

        Parameters
        ----------
        currency : `str`
            Cryptocurrency: BTC, ETH, BCH, LTC

        Returns
        -------
        
        """
        address = {}
        address['blockchainResponse'] = {}
        address['responseCode'] = 400
        address['message'] = ''
        if cryptocurrency == 'BTC':
            address = self.generate_btc_receiving_address()
        elif cryptocurrency == 'ETH':            
            address['message'] = CRYPTOCURRENCY_NOT_SUPPORTED
            address['blockchainResponse']['address'] = 'NA'
        elif cryptocurrency == 'BCH':
            address['message'] = CRYPTOCURRENCY_NOT_SUPPORTED
            address['blockchainResponse']['address'] = 'NA'
        elif cryptocurrency == 'LTC':
            address['message'] = CRYPTOCURRENCY_NOT_SUPPORTED
            address['blockchainResponse']['address'] = 'NA'
        else:
            address['message'] = CRYPTOCURRENCY_NOT_SUPPORTED
            address['blockchainResponse']['address'] = 'NA'
        print('***************** address: %s' % address)
        return address

    def generate_btc_receiving_address(self):
        """Generate a BTC receiving address
        """
        address = {}
        address['blockchainResponse'] = {}
        address['responseCode'] = 400
        address['message'] = ''
        try:
            # select the wallet to be used for generating the address
            pass
        except ObjectDoesNotExist:
            address['message'] = WALLET_NOT_FOUND
        return address

    def list_wallet(self, cryptocurrency):
        """Returns a list of wallets

        Parameters
        ----------
        currency : `str`
            Cryptocurrency: BTC, ETH, BCH, LTC

        Returns
        -------
        
        """
        wallets = {}
        wallets['blockchainResponse'] = {}
        wallets['responseCode'] = 400
        wallets['message'] = ''
        wallet_list = []
        if cryptocurrency == 'BTC':
            try:
                wallet_objects = BitcoinWallet.objects.all()
                for w in wallet_objects:
                    wallet_list.append(w.id)    
                #wallets['wallet_list'] = wallet_list
                wallets['blockchainResponse'] = wallet_list
                wallets['responseCode'] = 200
            except ObjectDoesNotExist as identifier:
                wallets['responseCode'] = 400
                wallets['message'] = WALLET_NOT_CREATED
                            
        elif cryptocurrency == 'ETH':            
            wallets['message'] = CRYPTOCURRENCY_NOT_SUPPORTED
        elif cryptocurrency == 'BCH':
            wallets['message'] = CRYPTOCURRENCY_NOT_SUPPORTED
        elif cryptocurrency == 'LTC':
            wallets['message'] = CRYPTOCURRENCY_NOT_SUPPORTED
        else:
            wallets['message'] = CRYPTOCURRENCY_NOT_SUPPORTED
        return wallets

    def send_money(self, wallet_id, cryptocurrency, to, amount, from_):
        """Send money to a specific address

        Parameters
        ----------
        wallet_id : `str`
            Wallet id
        cryptocurrency : `str`
            Cryptocurrency: BTC, ETH, BCH, LTC
        to : `str`
            address to send to
        amount : `str`
            Amount in Satoshi to send
        from_ : `str`
            sender address

        Returns
        -------
        
        """
        data = {}
        if cryptocurrency == 'BTC':
            try:
                wallet = BitcoinWallet.objects.get(pk=wallet_id)
                data = self._bitcoin_adapter.send_money(wallet.guid, wallet.password, 
                    to, amount, from_)
            except ObjectDoesNotExist:
                data['error'] = WALLET_NOT_FOUND
            
        elif cryptocurrency == 'ETH':            
            data['error'] = CRYPTOCURRENCY_NOT_SUPPORTED
        elif cryptocurrency == 'BCH':
            data['error'] = CRYPTOCURRENCY_NOT_SUPPORTED
        elif cryptocurrency == 'LTC':
            data['error'] = CRYPTOCURRENCY_NOT_SUPPORTED
        else:
            data['error'] = CRYPTOCURRENCY_NOT_SUPPORTED
        return data