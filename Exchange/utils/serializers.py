"""
Exchange platform
"""
## @package utils.serializers
#
# Serializers que permite maquetar los datos de los modelos de utils
# @author Team-Merida.
# @version 1.0
from rest_framework import serializers
from .models import *


class CiudadSerializer(serializers.ModelSerializer):
    class Meta:
        model = Ciudad
        fields = ('pk', 'nombre', 'estado')


class EstadoSerializer(serializers.ModelSerializer):
    class Meta:
        model = Estado
        fields = ('pk', 'nombre', 'pais')


class PaisSerializer(serializers.ModelSerializer):
    class Meta:
        model = Pais
        fields = ('pk', 'nombre')
