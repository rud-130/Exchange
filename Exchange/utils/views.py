"""
Exchange platform
"""
## @package utils.views
#
# Views that controls the processes of the utils
# @author Team-Merida.
# @version 1.0
import random, bitstamp.client


from bitcoinaverage import RestfulClient
from django.core import signing
from django.shortcuts import render

from forex_python.converter import CurrencyRates

from rest_framework import views, viewsets, permissions
from rest_framework.response import Response

from users.models import (
    TwoFactToken
)

from .constants import (
    SECRET_KEY_BTCAV, PUBLIC_KEY_BTCAV
)
from .serializers import *


class TokenGenerator():
    """!
    Token Generator, based on Django signing

    @author Rodrigo Boet (rudmanmrrod at gmail.com)
    @copyright
    @date 04-05-2017
    @version 1.0.0
    """

    def generate_token(self, user):
        """
        Generate token for user
        @param user Recives user objects
        @return token or False
        """
        try:
            token = signing.dumps(user.id)
            serial = random.randint(10000,99999)
            obj, created = TwoFactToken.objects.update_or_create(
                user=user,
                defaults={'token': token,'user': user,'serial':serial },
            )
            return str(serial)
        except:
            return False

    def check_token(self,user_id,token):
        """
        Check token for user
        @param user_id Recives user id
        @param token Recives token number
        @return Boolean
        """
        twofact = TwoFactToken.objects.filter(user_id=user_id)
        if(twofact):
            twofact = twofact.get()
            if(twofact.serial==int(token)):
                try:
                    signing.loads(twofact.token,max_age=350)
                    return True
                except:
                    return False
            else:
                return False
        else:
            return False


class ListarCiudadViewSet(viewsets.ViewSet):
    """!
    Vista simple para listar todos las ciudades almacenadas en el objeto Ciudad

    @author Ing. Leonel P. Hernandez M. (leonelphm at gmail.com)
    @copyright
    @date 10-05-2018
    @version 1.0.0
    """
    permission_classes = (permissions.IsAuthenticated, )
    queryset = Ciudad.objects.all()
    serializer_class = CiudadSerializer

    def list(self, request, format=None):
        """
            Método de la petición por get para listar los datos de las ciudades

            @param self Objeto que instancia el método
            @param request Objeto con la petición
            @param format Formato de la respuesta (json por omisión)
            @return Retorna los datos y almacena en base de datos
        """
        serializer = self.serializer_class(self.queryset, many=True)

        return Response(serializer.data)


class ListarEstadoViewSet(viewsets.ViewSet):
    """!
    Vista simple para listar todos las ciudades almacenadas en el objeto Estado

    @author Ing. Leonel P. Hernandez M. (leonelphm at gmail.com)
    @copyright
    @date 10-05-2018
    @version 1.0.0
    """
    permission_classes = (permissions.IsAuthenticated, )
    queryset = Estado.objects.all()
    serializer_class = EstadoSerializer

    def list(self, request, format=None):
        """
            Método de la petición por get para listar los datos de los estados

            @param self Objeto que instancia el método
            @param request Objeto con la petición
            @param format Formato de la respuesta (json por omisión)
            @return Retorna los datos y almacena en base de datos
        """
        serializer = self.serializer_class(self.queryset, many=True)

        return Response(serializer.data)


class ListarPaisViewSet(viewsets.ViewSet):
    """!
    Vista simple para listar todos las ciudades almacenadas en el objeto Pais

    @author Ing. Leonel P. Hernandez M. (leonelphm at gmail.com)
    @copyright
    @date 10-05-2018
    @version 1.0.0
    """
    permission_classes = (permissions.IsAuthenticated, )
    queryset = Pais.objects.all()
    serializer_class = PaisSerializer

    def list(self, request, format=None):
        """
            Método de la petición por get para listar los datos de los estados

            @param self Objeto que instancia el método
            @param request Objeto con la petición
            @param format Formato de la respuesta (json por omisión)
            @return Retorna los datos y almacena en base de datos
        """
        serializer = self.serializer_class(self.queryset, many=True)

        return Response(serializer.data)


class IpClient():
    """!
    Class to get the customers ip

    @author Ing. Leonel P. Hernandez M. (lhernandez at analiticom.com)
    @copyright ANALITICOM
    @date 19-04-2017
    @version 1.0.0
    """

    def get_client_ip(self, request):
        """
        Gets the ip address that makes requestsCheck token for user

        @param request Recives request
        @return ip
        """
        x_forwarded_for = request.META.get('HTTP_X_FORWARDED_FOR')
        if x_forwarded_for:
            ip = x_forwarded_for.split(',')[0]
        else:
            ip = request.META.get('REMOTE_ADDR')
        return ip

    def get_client_browser(self, request):
        """
        Gets the browser requestsCheck

        @param request Recives request
        @return browser
        """
        browser = request.META.get('HTTP_USER_AGENT')
        return browser


class CurrencyPricesView(views.APIView):
    """!
    Vista simple para listar los precios actules de las criptomonedas de la plataforma

    @author Ing. Leonel P. Hernandez M. (leonelphm at gmail.com)
    @copyright
    @date 19-05-2018
    @version 1.0.0
    """
    permission_classes = (permissions.IsAuthenticated, )

    def get(self, request, format=None):
        convert = CurrencyRates()
        usd_mxn = convert.get_rate('USD', 'MXN')
        #public_client = bitstamp.client.Public()
        restful_client = RestfulClient(SECRET_KEY_BTCAV, PUBLIC_KEY_BTCAV)
        # Caputra el precio actual de la cripto moneda por bitstamp
        #bitst = public_client.ticker('BTC', 'USD')['last']
        # Captura el diccionario local de la criptomoneda por bitcoinaverage
        bitcoinaverage_btc = restful_client.ticker_local_per_symbol('BTCMXN')['last']
        bitcoinaverage_bch = restful_client.ticker_local_per_symbol('BCHUSD')['last']*usd_mxn
        bitcoinaverage_eth = restful_client.ticker_local_per_symbol('ETHMXN')['last']
        bitcoinaverage_xrp = restful_client.ticker_local_per_symbol('XRPMXN')['last']
        precio = {'btc': bitcoinaverage_btc,
                  'bch': bitcoinaverage_bch,
                  'eth': bitcoinaverage_eth,
                  'xrp': bitcoinaverage_xrp,
                  'usd': usd_mxn}
        return Response(precio)
