"""
Exchange platform
Copyright (@) 2018
"""

## @package utils.urls
#
# Urls Router from utils
# @author Ing. Leonel P. Hernandez M.
# @version 1.0

from django.urls import path
from .views import *

urlpatterns = [
    path('precios/', CurrencyPricesView.as_view(), name='precios'),
]
