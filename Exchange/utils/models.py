"""
Exchange platform
"""
## @package utils.models
#
# Model that builds util data models
# @author Ing. Leonel P. Hernandez M.
# @version 1.0
from django.db import models


class Pais(models.Model):
    """!
    Clase que contiene el modelo de datos de Pais

    @author Ing. Leonel P. Hernandez M. (leonelphm at gmail.com)
    @copyright
    @date 13-04-2018
    @version 1.0.0
    """
    nombre = models.CharField(max_length=50)

    class Meta:
        ordering = ('nombre',)
        verbose_name = 'Pais'
        verbose_name_plural = 'Paises'

    def __str__(self):
        return self.nombre


class Estado(models.Model):
    """!
    Clase que contiene el modelo de datos del Estado

    @author Ing. Leonel P. Hernandez M. (leonelphm at gmail.com)
    @copyright
    @date 13-04-2018
    @version 1.0.0
    """
    nombre = models.CharField(max_length=50)
    pais = models.ForeignKey(Pais, on_delete=models.CASCADE)


    class Meta:
        ordering = ('nombre',)
        verbose_name = 'Estado'
        verbose_name_plural = 'Estados'


    def __str__(self):
        return self.nombre


class Ciudad(models.Model):
    """!
    Clase que contiene el modelo de datos de la Ciudad

    @author Ing. Leonel P. Hernandez M. (leonelphm at gmail.com)
    @copyright
    @date 13-04-2018
    @version 1.0.0
    """
    nombre = models.CharField(max_length=50)
    estado = models.ForeignKey(Estado, on_delete=models.CASCADE)

    class Meta:
        ordering = ('nombre',)
        verbose_name = 'Municipio'
        verbose_name_plural = 'Municipios'

    def __str__(self):
        return self.nombre


class Concept(models.Model):
    """!
    Clase que contiene el modelo de datos del concepto de pago

    @author Ing. Leonel P. Hernandez M. (leonelphm at gmail.com)
    @copyright
    @date 27-04-2018
    @version 1.0.0
    """
    name = models.CharField(max_length=50)

    class Meta:
        ordering = ('name',)
        verbose_name = 'Cocept'
        verbose_name_plural = 'Cocepts'

    def __str__(self):
        return self.name
