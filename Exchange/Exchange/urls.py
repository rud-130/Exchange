"""Exchange URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include

from rest_framework import routers

from users.views import *
from utils.views import (
    ListarCiudadViewSet, ListarEstadoViewSet,
    ListarPaisViewSet
)
from rest_framework.authtoken import views
from rest_framework_jwt.views import obtain_jwt_token

router = routers.DefaultRouter()
router.register(r'register/users', UserProfileViewSet)
router.register(r'list/ciudad', ListarCiudadViewSet)
router.register(r'list/estado', ListarEstadoViewSet)
router.register(r'list/pais', ListarPaisViewSet)
router.register(r'dispositivos', DispositivosViewSet)

urlpatterns = [
    path('api/', include(router.urls)),
    path('admin/', admin.site.urls),
    path('api/api-auth/', include('rest_framework.urls',
         namespace='rest_framework')),
    path('api/auth/', include('djoser.urls')),
    path('api/auth-token/', include('djoser.urls.authtoken')),
    path('api/api-token-auth/', obtain_jwt_token),
    path('api/', include('users.urls')),
    path('api/', include('utils.urls')),
]
