"""
Exchange platform
"""
## @package accountLedger.models
#
# Model that builds Accounting Ledger data models
# @author Ing. Leonel P. Hernandez M.
# @version 1.0
from django.db import models

from utils.models import Concept
from wallet.models import WalletUser


class LedgerWallet(models.Model):
    """!
        Clase que contiene el modelo de datos del libro mayor
    """
    fk_ledger = models.ForeignKey('self', on_delete=models.CASCADE)
    fk_wallet = models.ForeignKey(WalletUser, on_delete=models.CASCADE)
    fk_concept = models.ForeignKey(Concept, on_delete=models.CASCADE)
    date_action = models.DateTimeField(auto_now_add=True)
    debit = models.DecimalField(max_digits=20, decimal_places=8)
    credit = models.DecimalField(max_digits=20, decimal_places=8)
    total = models.DecimalField(max_digits=20, decimal_places=8)

    class Meta:
        """!
            Clase que construye los meta datos del modelo
        """
        ordering = ('date_action',)
        verbose_name = 'Ledger Wallet'
        verbose_name_plural = 'Ledger Wallets'

    def __str__(self):
        """!
        Función que muestra el concepto de la operacion

        @return Devuelve el username del usuario
        """
        return self.fk_concept.name
