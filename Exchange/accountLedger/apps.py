from django.apps import AppConfig


class AccountledgerConfig(AppConfig):
    name = 'accountLedger'
