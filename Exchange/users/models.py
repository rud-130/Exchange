"""
Exchange platform
"""
## @package users.models
#
# Model that builds user data models
# @author Ing. Leonel P. Hernandez M.
# @version 1.0
from django.contrib.auth.models import (
    Group, User
)
from django.db import models

from utils.models import (
    Pais
)


# A description field is added to the group model to describe the user group
# added by l.hernandez
Group.add_to_class('descripcion', models.TextField(blank=True))


class UserProfile(models.Model):
    """!
    Clase que contiene los perfiles de los usuarios

    @author Ing. Leonel P. Hernandez M. (leonelphm at gmail.com)
    @copyright
    @date 14-04-2018
    @version 1.0.0
    """
    fk_user = models.OneToOneField(User, on_delete=models.CASCADE)
    fk_pais = models.ForeignKey(Pais, on_delete=models.CASCADE)
    tipo_identificador = models.CharField(max_length=3)
    identificador = models.CharField(max_length=20, unique=True)
    zip_code = models.PositiveSmallIntegerField()
    ciudad = models.CharField(max_length=30)
    direccion = models.TextField()
    activo = models.BooleanField(default=True)

    class Meta:
        """!
            Clase que construye los meta datos del modelo
        """
        ordering = ('fk_user',)
        verbose_name = 'User Profile'
        verbose_name_plural = 'Users Profiles'
        db_table = 'users_user_profile'

    def __str__(self):
        """!
        Función que muestra el perfil del usuario

        @return Devuelve el username del usuario
        """
        return self.fk_user.username


class PhoneUser(models.Model):
    """!
    Clase que contiene los telefonos de los usuarios

    @author Ing. Leonel P. Hernandez M. (leonelphm at gmail.com)
    @copyright
    @date 14-04-2018
    @version 1.0.0
    """
    fk_user = models.ForeignKey(User, on_delete=models.CASCADE)
    codigo_zona = models.CharField(max_length=4)
    numero = models.PositiveSmallIntegerField()

    class Meta:
        """!
            Clase que construye los meta datos del modelo
        """
        ordering = ('numero',)
        verbose_name = 'Phone  user'
        verbose_name_plural = 'Phones Users'
        db_table = 'users_user_phone'

    def __str__(self):
        """!
        Función que muestra el numero del usuario

        @return Devuelve el numero del usuario
        """
        return str(self.numero)


class TwoFactToken(models.Model):
    """!
    Class for store Two Fact Token from user
    @author Rodrigo Boet (r.boet at eicabmx.com)
    @copyright
    @date 04-05-2017
    @version 1.0.0
    """
    # Relation to user model
    user = models.ForeignKey(User, on_delete=models.CASCADE)

    # Field to store Token
    token = models.CharField(max_length=128)

    # Field to store serial id
    serial = models.PositiveIntegerField()


class DispositivoUser(models.Model):
    """!
    Clase que la dirreccion de los dispositivos que usa el usuario el cual\
    se les da acceso

    @author Ing. Leonel P. Hernandez M. (leonelphm at gmail.com)
    @copyright
    @date 14-04-2018
    @version 1.0.0
    """
    fk_user = models.ForeignKey(User, on_delete=models.CASCADE)
    direccion_ip = models.CharField(max_length=15)
    acceso = models.BooleanField(default=False)
    nombre = models.CharField(max_length=128)
    navegador = models.CharField(max_length=128)

    class Meta:
        """!
            Clase que construye los meta datos del modelo
        """
        ordering = ('direccion_ip',)
        unique_together = (("fk_user", "direccion_ip", 'navegador'),)
        verbose_name = 'Dispositivo User'
        verbose_name_plural = 'Dispoositivos del Usuario'

    def __str__(self):
        """!
        Función que muestra la direccion ip de equipo

        @return Devuelve la direccion ip de equipo
        """
        return self.direccion_ip
