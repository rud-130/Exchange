"""
Exchange platform
"""
## @package users.serializers
#
# Serializes the data of the models and data necessary for the api of users
# @author Ing. Leonel P. Hernandez M.
# @version 1.0
from django.contrib.auth import (
    authenticate, password_validation
)
from djoser.serializers import UserCreateSerializer
from rest_framework import serializers

from utils.views import TokenGenerator

from .models import *


class UserSerializer(serializers.ModelSerializer):
    """!
    Clase que serializa los datos del usuario

    @author Ing. Leonel P. Hernandez M. (leonelphm at gmail.com)
    @copyright
    @date 10-05-2018
    @version 1.0.0
    """
    class Meta:
        model = User
        fields = ('pk', 'username', 'first_name',
                  'last_name', 'email', 'last_login',
                  'date_joined')


class UserProfileSerializer(serializers.ModelSerializer):
    """!
    Clase que serializa los datos del perfil de usuario

    @author Ing. Leonel P. Hernandez M. (leonelphm at gmail.com)
    @copyright
    @date 10-05-2018
    @version 1.0.0
    """
    class Meta:
        model = UserProfile
        fields = ('pk', 'fk_pais', 'tipo_identificador', 'identificador',
                  'zip_code', 'ciudad', 'direccion', 'activo')


class CreateUserSerializer(UserCreateSerializer):
    """!
    Clase que sobreescribe los meta datos de la clase UserCreateSerializer

    @author Ing. Leonel P. Hernandez M. (leonelphm at gmail.com)
    @copyright
    @date 27-04-2018
    @version 1.0.0
    """

    class Meta:
            model = User
            fields = tuple(User.REQUIRED_FIELDS) + (
                User.USERNAME_FIELD, User._meta.pk.name, 'password',
                'first_name', 'last_name'
            )


class LoginOneTimeSerializer(serializers.Serializer):
    """!
    Clase que serializa el formulario para el login por dos fases

    @author Ing. Leonel P. Hernandez M. (leonelphm at gmail.com)
    @copyright
    @date 04-05-2018
    @version 1.0.0
    """
    username = serializers.CharField(label=("Username"))
    password = serializers.CharField(label=("Password"),
                                     style={'input_type': 'password'})

    def validate(self, attrs):
        """
        Metodo que valida los parametros enviados al serializer
        @param attrs objeto que contiene los valores de los capos serializados
        @return attrs objeto que contiene los atributos del serializador
        """
        username = attrs.get('username')
        password = attrs.get('password')
        if username and password:
            user = authenticate(username=username, password=password)

            if user:
                # From Django 1.10 onwards the `authenticate` call simply
                # returns `None` for is_active=False users.
                # (Assuming the default `ModelBackend` authentication backend.)
                if not user.is_active:
                    msg = ('User account is disabled.')
                    raise serializers.ValidationError(msg)
            else:
                msg = ('Unable to log in with provided credentials.')
                raise serializers.ValidationError(msg)
        else:
            msg = ('Must include "username" and "password".')
            raise serializers.ValidationError(msg)

        attrs['user'] = user
        return attrs


class LoginTwoTimeSerializer(serializers.Serializer):
    """!
    Clase que serializa el formulario para el login por dos fases

    @author Ing. Leonel P. Hernandez M. (leonelphm at gmail.com)
    @copyright
    @date 04-05-2018
    @version 1.0.0
    """
    username = serializers.CharField(label=("Username"))
    token = serializers.IntegerField(label=("Token"))

    def validate(self, attrs):
        """
        Metodo que valida los parametros enviados al serializer
        @param attrs objeto que contiene los valores de los capos serializados
        @return attrs objeto que contiene los atributos del serializador
        """
        valid_token = TokenGenerator()
        username = attrs.get('username')
        token = attrs.get('token')
        if len(str(token)) != 5:
            msg = ('Invalid Token.')
            validar = False
            raise serializers.ValidationError(msg)
        else:
            try:
                user = User.objects.get(username=username)
            except:
                user = None
            validar = valid_token.check_token(user.id, token)
            if not validar:
                msg = ('Invalid Token.')
                raise serializers.ValidationError(msg)
        attrs['validar'] = validar
        attrs['user'] = user
        return attrs


class CambiarPasswordSerializer(serializers.Serializer):
    """!
    Clase que serializa el formulario para cambiar la password

    @author Ing. Leonel P. Hernandez M. (leonelphm at gmail.com)
    @copyright
    @date 10-05-2018
    @version 1.0.0
    """
    pk_user = serializers.CharField(label=("Pk User"))
    password_old = serializers.CharField(label=("Password Anterior"),
                                     style={'input_type': 'password'})
    password_new = serializers.CharField(label=("Nueva Contraseña"),
                                     style={'input_type': 'password'})

    def validate(self, attrs):

        pk_user = attrs.get('pk_user')
        password_old = attrs.get('password_old')
        password_new = attrs.get('password_new')
        if password_old and password_new:
            try:
                # validate the password and catch the exception
                user = User.objects.get(pk=pk_user)
                password_validation.validate_password(password_old)
                # the exception raised here is different than serializers.ValidationError
                msg = "La constraseña anterior coincide puedes cambiarla"
                valida = True
            except Exception as e:
                user = None
                msg = 'La password no corresponde a tu password actual'
                valida = False
        else:
            user = None
            msg = 'Must include "pk user", "password old" y " new password"'
            valida = False
            raise serializers.ValidationError(msg)

        attrs['valida'] = valida
        attrs['user'] = user
        attrs['password_new'] = password_new
        attrs['msg'] = msg
        return attrs


class DispositivoUserSerializer(serializers.ModelSerializer):
    """!
    Clase que serializa los datos del dispositivo del usuario

    @author Ing. Leonel P. Hernandez M. (leonelphm at gmail.com)
    @copyright
    @date 11-05-2018
    @version 1.0.0
    """
    class Meta:
        model = DispositivoUser
        fields = ('pk', 'direccion_ip', 'acceso', 'nombre', 'navegador')
