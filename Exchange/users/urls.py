"""
Exchange platform
Copyright (@) 2018
"""

## @package users.urls
#
# Urls Router from users
# @author Ing. Leonel P. Hernandez M.
# @version 1.0

from django.urls import path
from .views import *

urlpatterns = [
    path('login/one-time/', ObtainAuthToken.as_view(), name='login_onetime'),
    path('login/validate/', ValidateToken.as_view(), name='login_validate'),
    path('user/change-password/', CambiarPassword.as_view(), name='change_pass'),
    path('user/config/', ConfigurarDispositivo.as_view(), name='config_user'),
]
