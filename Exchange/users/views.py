"""
Exchange platform
"""
## @package users.views
#
# Views that controls the processes of the users
# @author Team-Merida.
# @version 1.0
import socket
from django.core.mail import send_mail
from django.shortcuts import render

from rest_framework import views, viewsets, permissions
from rest_framework.response import Response

from utils.views import (
    TokenGenerator, IpClient
)
from .models import *
from .serializers import *


class UserProfileViewSet(viewsets.ModelViewSet):
    """!
    Vista para crear el perfil del usuario

    @author Ing. Leonel P. Hernandez M. (leonelphm at gmail.com)
    @copyright
    @date 04-05-2018
    @version 1.0.0
    """
    model = UserProfile
    queryset = UserProfile.objects.all()
    serializer_class = UserProfileSerializer
    permission_classes = (permissions.IsAuthenticated, )

    def list(self, request, format=None):
        """
            Método de la petición por get para listar los datos del perfil de usuario
            @param self Objeto que instancia el método
            @param request Objeto con la petición
            @param format Formato de la respuesta (json por omisión)
            @return Retorna los datos y almacena en base de datos
        """
        pk = request.GET.get('pk')
        if pk is not None:
            try:
                queryset = self.model.objects.get(fk_user=pk)
            except Exception as e:
                queryset = []
                return Response(queryset)
            serializer = self.serializer_class(queryset)

            return Response(serializer.data)
        else:
            serializer = self.serializer_class(self.queryset, many=True)
            return Response(serializer.data)

    def create(self, request):
        """
    	Metodo para crear el perfil del usuario recibe los parametros del modelo
    	"""
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        user = User.objects.get(pk=request.data['pk'])
        serializer.save(fk_user=user)

        return Response(serializer.data)


class ObtainAuthToken(views.APIView):
    """!
    Clase que obtiene el token para el logeo por dos etapas

    @author Ing. Leonel P. Hernandez M. (leonelphm at gmail.com)
    @copyright
    @date 04-05-2018
    @version 1.0.0
	"""
    serializer_class = LoginOneTimeSerializer
    permission_classes = ()

    def post(self, request, *args, **kwargs):
        """!
        Metodo que recibe los parametros de la petición realizada por post\
        envia el mensaje al correo electronico del usuario resgistrado que \
        esta intentando iniciar sesión.

        @author Ing. Leonel P. Hernandez M. (leonelphm at gmail.com)
        @copyright
        @date 04-05-2018
        @param request <b>{object}</b> Objects con la peticion
        @param args <b>{object}</b> Objects con parametros del contexto
        @param kwargs <b>{object}</b> Objects con parametros del contexto
        @return responde el token
        """
        new_token = TokenGenerator()
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        user = serializer.validated_data['user']
        token = new_token.generate_token(user)
        if token:
            msj_mail = 'this is the security token that you must enter before\
                        5 minutes: %s' % (token)
            try:
                send_mail(
                            'Validate Security',
                            msj_mail,
                            'leogck12@gmail.com',
                            [user.email],
                            fail_silently=False,
                        )
            except Exception as e:
                print(e)

        return Response({'token': token})


class ValidateToken(views.APIView):
    """!
    Clase que obtiene valida el token para el logeo por dos etapas

    @author Ing. Leonel P. Hernandez M. (leonelphm at gmail.com)
    @copyright
    @date 04-05-2018
    @version 1.0.0
    """
    serializer_class = LoginTwoTimeSerializer
    permission_classes = ()

    def post(self, request, *args, **kwargs):
        """!
        Metodo que recibe los parametros de la petición realizada por post\
        valida el token y permite dar inicio la sesión del usuario en la\
        plataforma.

        @author Ing. Leonel P. Hernandez M. (leonelphm at gmail.com)
        @copyright
        @date 04-05-2018
        @param request <b>{object}</b> Objects con la petición
        @param args <b>{object}</b> Objects con parametros del contexto
        @param kwargs <b>{object}</b> Objects con parametros del contexto
        @return responde la validación del token True o False
        """
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        token = serializer.validated_data['validar']
        user = serializer.validated_data['user']
        user_serializer = UserSerializer(user)

        return Response({'validar': token, 'user': user_serializer.data})


class CambiarPassword(views.APIView):
    """!
    Clase que obtiene valida el token para el logeo por dos etapas

    @author Ing. Leonel P. Hernandez M. (leonelphm at gmail.com)
    @copyright
    @date 10-05-2018
    @version 1.0.0
    """
    serializer_class = CambiarPasswordSerializer
    permission_classes = (permissions.IsAuthenticated, )

    def post(self, request, *args, **kwargs):
        """!
        Metodo que recibe los parametros de la petición realizada por post\
        valida el password y permite cambiar la constraseña del usuario

        @author Ing. Leonel P. Hernandez M. (leonelphm at gmail.com)
        @copyright
        @date 10-05-2018
        @param request <b>{object}</b> Objects con la petición
        @param args <b>{object}</b> Objects con parametros del contexto
        @param kwargs <b>{object}</b> Objects con parametros del contexto
        @return responde la validación del token True o False
        """
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        valida = serializer.validated_data['valida']
        msg = serializer.validated_data['msg']
        if valida:
            user = serializer.validated_data['user']
            password_new = serializer.validated_data['password_new']
            try:
                user.set_password(password_new)
                user.save()
                msg += "Su contraseña se cambio con exito"
            except Exception as e:
                print(e)
                msg = e
                valida = False
            else:
                pass
            finally:
                pass

        return Response({'valida': valida, 'msg': msg})


class DispositivosViewSet(viewsets.ModelViewSet):
    """!
    Vista simple para lista, crear, actualizar y eliminar los dispositvos del user

    @author Ing. Leonel P. Hernandez M. (leonelphm at gmail.com)
    @copyright
    @date 11-05-2018
    @version 1.0.0
    """
    model = DispositivoUser
    queryset = DispositivoUser.objects.all()
    serializer_class = DispositivoUserSerializer
    permission_classes = (permissions.IsAuthenticated, )

    def list(self, request, format=None):
        """
            Método de la petición por get para listar los datos del perfil de usuario

            @param self Objeto que instancia el método
            @param request Objeto con la petición
            @param format Formato de la respuesta (json por omisión)
            @return Retorna los datos y almacena en base de datos
        """
        pk = request.GET.get('pk')
        if pk is not None:
            try:
                queryset = self.model.objects.filter(fk_user=pk)
            except Exception as e:
                queryset = []
                return Response(queryset)
            serializer = self.serializer_class(queryset, many=True)

            return Response(serializer.data)
        else:
            serializer = self.serializer_class(self.queryset, many=True)
            return Response(serializer.data)

    def create(self, request):
        """!
            Método para almacenar los dispositivos del usuario

            @author Ing. Leonel P. Hernandez M. (leonelphm at gmail.com)
            @copyright
            @date 19-05-2018
            @param request <b>{object}</b> Objects con la peticion
            @return responde serializer.data
        """
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        user = User.objects.get(pk=request.data['pk'])
        serializer.save(fk_user=user)

        return Response(serializer.data)


class ConfigurarDispositivo(views.APIView):
    """!
    Clase para configurar el dispositivo del user

    @author Ing. Leonel P. Hernandez M. (leonelphm at gmail.com)
    @copyright
    @date 11-05-2018
    @version 1.0.0
    """
    ip = IpClient
    permission_classes = (permissions.IsAuthenticated, )

    def post(self, request, *args, **kwargs):
        """!
        Metodo que recibe el parametro del user para configurar el dispositivo

        @author Ing. Leonel P. Hernandez M. (leonelphm at gmail.com)
        @copyright
        @date 11-05-2018
        @param request <b>{object}</b> Objects con la petición
        @param args <b>{object}</b> Objects con parametros del contexto
        @param kwargs <b>{object}</b> Objects con parametros del contexto
        @return responde los parametros del dispositivo
        """
        pk = request.data['pk']
        get_ip_client = self.ip()
        direccion_ip = get_ip_client.get_client_ip(request)
        browser = get_ip_client.get_client_browser(request)
        nombre = socket.gethostbyaddr(direccion_ip)[0]
        return Response({'ip': direccion_ip, 'nombre_equipo': nombre,
                         'navegador': browser})
